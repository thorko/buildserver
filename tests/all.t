#!perl

use lib 'lib';
use Buildctl::Base;
use Config::Simple;
use File::Grep qw(fgrep);
use File::Slurp qw(edit_file_lines);
use Test::More;
use FindBin qw($Bin);
use strict;
use warnings;

my $opt = "-Mlib=$Bin/../lib";
my $config = "tests/buildctl.conf";
my $cfgopt = "-c $config";
my $tool = "$^X $opt $Bin/../buildctl.pl $cfgopt";

my $srv = "$^X $opt $Bin/../buildsrv.pl -c tests/buildsrv.conf";

like(qx/$tool -h/, qr/requires: --option/, 'check help message');
like(qx/$tool -r help/, qr/use build file to install app/, 'check pod2usage');

# list-versions
like(qx/$tool -r list -o version/, qr/php5:.*0.0.1.*0.0.2/, 'list application versions');
# list-version of app
like(qx/$tool -r list -o version -a apache2/, qr/apache2:.*1.2.0.*1.2.1/, 'list version of app apache2');

# get-active
like(qx/$tool -r get-active/, qr/apache2: 1.2.0/, 'get active version');
like(qx/$tool -r get-active -a apache2/, qr/apache2: 1.2.0/, 'get active version of apache');

# switch version
like(qx/$tool -r activate -a apache2 -v 1.2.1/, qr/apache2: activated 1.2.1/, 'switched to version 1.2.1');
like(qx/$tool -r activate -a apache2 -v 1.2.0/, qr/apache2: activated 1.2.0/, 'switched to version 1.2.0');
like(qx/$tool -r activate -a apache2 -v 1.2.0/, qr/WARN: start-stop script couldn't be found/, 'check warning of service action');

# pack an app
like(qx/$tool -r pack -a apache2 -v 1.2.1/, qr/Packaging apache2 1.2.1:\t\[OK\]/, 'pack app apache2');
qx{rm -f /tmp/apache2/apache2-1.2.1.tar.gz};

# update an app
like(qx/$tool -r update -b tests\/mariadb.conf/, qr/Updating app: mariadb to 5.5.56/, 'update app mariadb');

# test rep_var
my $hash = { install_path => '/usr/local/%app/%version', app => 'bind', version => '9.10.4-P8'};
my $b = Buildctl::Base->new(config => $config, debug => 0);
is($b->rep_var('/usr/local/%app/%version', $hash), '/usr/local/bind/9.10.4-P8', 'test path variable expansion');
# test app config file expansion
my $c = new Config::Simple();
$c->read("tests/mariadb.conf");
my $buildhash = $c->get_block("config");

is($b->rep_var($buildhash->{'install_path'}, $buildhash), '/usr/local/mariadb/5.5.56', 'test build file expansion 1');
is($b->rep_var($buildhash->{'url'}, $buildhash), 'https://downloads.mariadb.org/f/mariadb-5.5.56/source/mariadb-5.5.56.tar.gz/from/http%3A//ftp.hosteurope.de/mirror/mariadb.org/?serve', 'test build file expansion 2');
is($b->rep_var($buildhash->{'make'}, $buildhash), 'make %test', 'test failed macro expansion');

is($b->switch_version("", ""), 1, 'switch_version without app');
is($b->switch_version("apache2", "1.9.0"), 1, 'switch_version version not available');
is($b->switch_version("apache2", "1.9.0"), 1, 'switch_version version not available');

is($b->download("http://tt.tt/tt.tar.gz", "/tmp/a.tar.gz"), 1, 'failed download');

# test build file missing
is($b->build(""), 1, 'test build file missing');
is($b->build("/tmp/t"), 1, 'test build file does not exist');

# pack
is($b->pack("apache2", ""), 1, "pack - required options missing");
is($b->pack("apache2", "1.2.0"), 0, "pack - couldn't pack");

# test build script expansion
like(qx{$tool -r build -b tests/mariadb.conf}, qr{Run your build script /tmp/test_mariadb/mariadb.sh:\tERROR: check your build script and log /tmp/mariadb-build.log}, 'create build script');
my $scriptfile = "/tmp/test_mariadb/mariadb.sh";
ok((fgrep { /https.*mariadb-5\.5\.56\.tar\.gz/ } $scriptfile) == 1, 'found version in url');
ok((fgrep { /configure.*mariadb.5\.5\.56 --with/ } $scriptfile) == 1, 'found version in configure line');
# clean up
qx{rm -rf /tmp/test_mariadb};

# test full build script expansion
qx{$tool -r build -b tests/php.conf};
$scriptfile = "/tmp/php7/php.sh";
ok((fgrep { /touch \/tmp\/php7\/1.0.2l/ } $scriptfile) == 1, 'found openssl version in script');
ok((fgrep { /echo \/usr\/local\/openssl\/1.0.2l/ } $scriptfile) == 1, 'found openssl version in script');

# clean up
qx{rm -rf /tmp/php7};

like(qx{$tool -r build -b tests/missing_variable.conf}, qr{ERROR: Missing mandatory config variable}, 'test with missing mandatory variable');
like(qx{$tool -r build -b tests/missing_buildvar.conf}, qr{Missing build_opts or build_script in config file}, 'test with missing build variable');

# test web
# prepare
qx{rm -f tests/opensource.conf};
qx{cp tests/zabbix.conf.t tests/zabbix.conf};
like(qx{$tool -r web}, qr{ERROR: Config file .* does not exist}, 'test web - config file missing');
qx{cp tests/opensource_missing.conf.t tests/opensource.conf};
like(qx{$tool -r web}, qr{ERROR: Missing parameter in config file for}, 'test web - missing parameter');
qx{cp tests/opensource.conf.t tests/opensource.conf};
like(qx{$tool -r web}, qr{INFO: updated buildfile}, 'test web - test update');
qx{rm -f tests/opensource.conf};
qx{cp tests/zabbix.conf.t tests/zabbix.conf};

my $package_file = "tests/repository/.package_info";
my $uri_404 = "http://127.0.0.1:12355/t";

# start server
my $pid = qx($srv > /dev/null 2>&1 & echo \$!);

like(qx/$tool -r repository/, qr/nginx/, 'get repository');
like(qx/$tool -r repository -a nginx/, qr/nginx-1.12.0.tar.gz\tignore/, 'show repository');
like(qx/$tool -r repository -a apache2/, qr/apache2-1.2.0.tar.gz/, 'show repository without marked packages');

# test 404
my $out = qx{/usr/bin/curl -s -q $uri_404};
like($out, qr/404 Not Found/, 'test 404 error code');

# mark package
like(qx/$tool -r mark -a test -v 0.0.9 -m k/, qr{ERROR: tests/repository/test/test-0.0.9.tar.gz is not in repository}, 'mark nonexisting package');
like(qx/$tool -r mark -a bind -v 9.10.4-P6 -m k/, qr{Marked tests/repository/bind/bind-9.10.4-P6.tar.gz as keep}, 'mark package bind');
like(qx/$tool -r mark -a openssl -v 1.0.2l -m u/, qr{ERROR: Only k, f, i are allowed}, 'mark package openssl with u');
like(qx/$tool -r mark -a apache -v 1.13.0 -m k/, qr{Marked tests/repository/apache/apache-1.13.0.tar.gz as keep}, 'mark package test with k');
# cleanup
edit_file_lines { $_ = "" if /k tests\/repository\/apache\/apache-1.13.0.tar.gz/ } $package_file;

# list package state
like(qx/$tool -r list -o package_state/, qr{tests/repository/openssl/openssl-1.0.2l.tar.gz\tkeep}, 'list marked packages');

# install nginx
like(qx/$tool -r install -t nginx-1.12.0.tar.gz/, qr{/nginx/nginx-1.12.0.tar.gz is set to ignore}, 'try to install nginx');
like(qx/$tool -r install -t nginx-1.12.0.tar.gz -f/, qr/installing: nginx-1.12.0.tar.gz.*OK/, 'install nginx');
like(qx/$tool -r install -t mailsrv-1.12.0.tar.gz/, qr/ERROR: mailsrv-1.12.0.tar.gz not available in repository/, 'app not available in repository');

like(qx/$tool -r install -t apache-1.13.0.tar.gz /, qr{tests/repository/apache/apache-1.12.0.tar.gz is set to keep}, 'test pinned package apache - error');
like(qx/$tool -r install -t bind-9.10.4-P8.tar.gz/, qr{tests/repository/bind/bind-9.10.4-P6.tar.gz is set to keep}, 'test pinned package bind - error');
# install kept package
like(qx/$tool -r install -t bind-9.10.4-P6.tar.gz/, qr{installing: bind-9.10.4-P6}, 'test kept package bind');

# test latest
like(qx/$tool -r install -t nginx -v latest -f/, qr/installing: nginx-1.12.0.tar.gz.*OK/, 'install latest nginx');

# delete nginx
like(qx/$tool -r delete -a nginx -v 1.12.0/, qr/nginx: delete 1.12.0.*OK/, 'delete nginx');
like(qx/$tool -r delete -a apache2 -v 1.2.0/, qr/ERROR: apache2: can't delete active version 1.2.0/, 'delete active version apache2');

# cleanup
qx(rm -rf tests/apps/nginx);
qx(kill -HUP $pid);

# test build with app config file
$pid = qx($srv > /dev/null 2>&1 & echo \$!);
my $build_output = qx{$tool -r build -b tests/apache.conf};
#like($build_output, qr{Requirements installed: ERROR}, 'install build requirements');
like($build_output, qr{Will download http://localhost:12355/nginx/nginx-1.12.0.tar.gz:.*OK}, 'test download of build');
like($build_output, qr{Extract archive /tmp/app.tgz to /tmp/apache2:.*OK}, 'test extract of downloaded source');
like($build_output, qr{Running pre command:.*OK}, 'test prebuild command');
like($build_output, qr{Configure:.*OK}, 'test configure of source');
like($build_output, qr{Make:.*OK}, 'test make of source');
like($build_output, qr{Install:.*OK}, 'test install of source');
like($build_output, qr{Running post command:.*OK}, 'test prebuild command');
# cleanup
qx(kill -HUP $pid);

done_testing();
